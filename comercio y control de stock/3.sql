create table ventas(
nro int
);

create sequence sq_ventas start with 1 increment by 1;
--CREATE SEQUENCE customers_seq
 --START WITH     1000
 --INCREMENT BY   1
 
 
create table pedido(
cart_id int,
cli_id int,
precio decimal(10)
);

alter table pedido add constraint pk_pedido primary key (cart_id,cli_id);



create table product(
prod_id int,
descrip varchar(10),
precio decimal(10)
);

alter table product add constraint pk_product primary key (prod_id);



create table list_prod(
cart_id int,
cli_id int,
prod_id int
);


alter table list_prod add constraint fk_ls_prod foreign key (prod_id) references product(prod_id);



create table cliente(
cli_id int
);
