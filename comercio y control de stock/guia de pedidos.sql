

-- total a pagar de el cliente 2 :

select sum(pe.precio) from product pe,list_prod ls where ls.cli_id = 2 and pe.prod_id = ls.prod_id;


-- total a pagar de el cliente 1 :

select sum(pe.precio) from product pe,list_prod ls where ls.cli_id = 1 and pe.prod_id = ls.prod_id;

select * from list_prod;
-- crear na function q devuelva la suma d los productos a determinado cliente x su id:

create or replace function total (nro in  product.prod_id%type)
return decimal as resultado product.precio%type;
begin
select sum(pr.precio) into resultado from product pr,list_prod ls where  pr.prod_id = ls.prod_id and  ls.cli_id = nro;
--select sum(pe.precio) from product pe,list_prod ls where ls.cli_id = 2 and pe.prod_id = ls.prod_id;
return resultado;
end total;


-- testear funcion:

select total(2) from dual;

select total(1) from dual;

-- test : OK


-- test nro d ventas. esto sucede cuando se arma un pepido. primero hay q armar el pedido:

insert into pedido values (1,1,total(1));
insert into pedido values (2,2,total(2));

-- el pedido tiene que estar armado con el total a pagar de los producto comprados x los clientes. hay q comprovarlo:

select * from pedido;

-- tets OK.

-- crear nuevamente la seq para saber el tottal de ventas...

-- drop sequence sq_ventas; 

--borrrar los reg d pedido para q vuelva a empezar d nuevo todo:

delete from pedido;

--create sequence sq_ventas start with o increment by 1 minvalue 0;

CREATE SEQUENCE sq_ventas 
  START WITH 1 
  INCREMENT BY 1 
  MINVALUE 0;


-- el dia de hoy tuvimos:

select max(nro) from ventas;
select * from ventas;

-- hay que vender mas...

insert into list_prod values (3,3,4);
insert into list_prod values (3,3,12);
insert into pedido values (3,3,total(3));

insert into list_prod values (4,4,4);
insert into list_prod values (4,4,12);
insert into pedido values (4,4,total(4));


insert into list_prod values (5,5,4);
insert into list_prod values (5,5,12);
insert into pedido values (5,5,total(5));

select count(*) from pedido;
select * from pedido where cli_id = 3;


-- agregamos un campo mas a la tabla cliete para saber sus nombres:

alter table cliente  add  nombre varchar(20);

-- agregamos nombres a los clientes que compraron:

insert into cliente values(1,'cli 1');
insert into cliente values(2,'cli 2');
insert into cliente values(3,'cli 3');
insert into cliente values(4,'cli 4');
insert into cliente values(5,'cli 5');

-- queremos el nombre d cada cliente x compra:

select cl.nombre, pe.cart_id,pe.precio from pedido pe, cliente cl where cl.cli_id = pe.cli_id;



-- lista de producto que compro el cliente 1  cliente..


select ls.cli_id,pr.descrip from product pr, list_prod ls where pr.prod_id = ls.prod_id and ls.cli_id = 1 ;



-- lista de producto que compro el cliente 2  cliente..


select ls.cli_id,pr.descrip from product pr, list_prod ls where pr.prod_id = ls.prod_id and ls.cli_id = 2 ;


-- RAW(size) 


create or replace function productos_cli (nro in  cliente.cli_id%type)
return resultado%rowtype as resultado devtype%rowtype;
begin
select pr.descrip into resultado from product pr,list_prod ls where  pr.prod_id = ls.prod_id and  ls.cli_id = nro;
return resultado;
end productos_cli;
   

create type devtyppe as object(
cmap1 int
);


-- testear : 
select productos_cli(5) from dual;


-- creat una tabla de estock:

create table stock(
prod_id int,
cantidad int
);

create table prod_cod(
prod_id int,
prod_cod int
);

insert into prod_cod values (12,34);
insert into prod_cod values(12,45);
insert into prod_cod values(12,47);
insert into prod_cod values(12,77);
insert into prod_cod values(3,42);
insert into prod_cod values(3,34);
insert into prod_cod values(4,1);
--crear function que devuelva la cantidad d ese mismo prod:

create or replace function cantidad_prod (nro in  product.prod_id%type)
return int as resultado stock.cantidad%type;
begin
select count(prod_id) into resultado from prod_cod pr where pr.prod_id = nro;
return resultado;
end cantidad_prod;

--select count(prod_id) from prod_cod pr where pr.prod_id = 3;

-- testear function : 

select cantidad_prod(3) from dual;
select cantidad_prod(12) from dual;
select cantidad_prod(4) from dual;

-- crear registros en la tabla stock que indiquen cantidad de stock de cada producto:

insert into stock values(12,cantidad_prod(12));
insert into stock values(3,cantidad_prod(3));
insert into stock values(4,cantidad_prod(4));

-- comprobar que todo funcione correctamete:

select * from stock;

-- siguiente paso seria automatizar...

/*
create or replace trigger upd_stock
before insert on prod_cod
for each row
begin

end upd_stock;
*/