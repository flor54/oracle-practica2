-- sistema hospital

create table pasciente(
dni int
-- ... datos personales...
);
/*
create table hist_clinica(
legajo_id int,
codigo_consultas int
);
*/ 
-- hist clinica podria ser una vista o una function q muestre los turnos q tuvo cada pasciente en el hospital
create table doctor(
matricula int,
agenda int
-- datos personales...
);

create table turno(
cod_turno int,
pasciente int,
doctor int,
id_especialidad int ,
fecha timestamp,
nro_consultorio int,
costo decimal(10),
atendido char(1)
);


create table especialidad(
id_especialidad int,
descrip varchar(20)
);

create table agenda(
cod_turno int
);


create table consultorio(
consult_id int
);

--create or replace constraint 

alter table turno add constraint pk_turno primary key (cod_turno);

alter table turno add constraint fk_turno_dr foreign key (doctor) references doctor(matricula);

alter table turno add constraint fk_tr_pasciente foreign key (pasciente) references pasciente(dni);



alter table turno add constraint fk_tr_especialidad foreign key (id_especialidad) references consultorio(id_especialidad);


alter table turno add constraint fk_tr_consultorio foreign key (nro_consultorio) references especialidad(consult_id);




