create or replace trigger ventas_upd
 after insert 
 on pedido
 for each row
 begin
 insert into ventas values(sq_ventas.nextval);
 end ventas_upd;


create or replace trigger total_upd3
before insert on pedido
for each row
declare

end total_upd3;

create or replace trigger total_upd2
before insert on list_prod
for each row
declare
 cli int;
 cart int;
begin

  select cli_id into cli from pedido where :new.cli_id in (select cli_id from list_prodAux);
  select cart_id into cart from pedido where :new.cart_id in (select cart_id from list_prodAux);
  
 if  cart is null and cli is null then
 insert into pedido values (:new.cart_id, :new.cli_id);
 
 end if;

end total_upd2;

create or replace trigger total_upd2
before insert on list_prod
for each row
declare
 toal_precio decimal(10);

 --total(:new.prod_id)
 
 --select sum(pr.precio) into resultado from product pr,pedido pe where pe.cli_id in ( select ls.cli_id from list_prodAux ls);
 cli int;
 cart int;
 begin
 --SELECT * INTO some_employee FROM employees WHERE ROWNUM < 2;
 --select sum(precio) into toal_precio from pedido where prod_id = :new.prod_id;
  select sum(pr.precio) into toal_precio from product pr,pedido pe where :new.cli_id in ( select ls.cli_id from list_prodAux ls);
  select cli_id into cli from pedido where :new.cli_id in (select cli_id from list_prodAux);
  select cart_id into cart from pedido where :new.cart_id in (select cart_id from list_prodAux);
  
 if  cart is null and cli is null then
 
 insert into pedido values (:new.cart_id,:new.cli_id,toal_precio);
 
 else 
  update pedido set precio = toal_precio where cli_id = :new.cli_id;
 end if;
 
 end total_upd2;
 
 --insert into pedido values(:old.card_id,toal_precio);
/*
  merge into pedido pe using clente ls
 on (pe.cli_id = ls.cli_id)
 when matched then
 update set precio = toal_precio where pe.cli_id = :new.cli_id
 when not matched then
 insert (cart_id,cli_id,precio)values(:new.card_id, :new.cli_id, toal_precio);
 */
 drop trigger total_upd2
 
 
 drop trigger total_upd;
 
 

 /*
 

 
 MERGE INTO clientes cli USING datos_cli dac
ON (cli.cliente_id = dac.cliente_id)
WHEN MATCHED THEN
UPDATE SET 
cli.nombre = dac.nombre,
cli.direccion = dac.direccion
WHEN NOT MATCHED THEN
INSERT (cliente_id, nombre, direccion)
VALUES (dac.cliente_id, dac.nombre, dac.direccion);
 */
drop trigger upd_aux1;
drop trigger upd_aux2
 
create or replace trigger upd_aux1
after insert on list_prod
for each row
begin
insert into list_prodAux values(:new.cart_id,:new.cli_id,:new.prod_id);
commit;
end upd_aux1;

create or replace trigger upd_aux2
after update of prod_id on list_prod
for each row
begin
update list_prodAux set prod_id = :new.prod_id where cli_id = :old.cli_id;
commit;
end upd_aux2;


create or replace trigger total_upd
before insert on list_prod
for each row
declare
 toal_precio int;
 begin
 --SELECT * INTO some_employee FROM employees WHERE ROWNUM < 2;
 select sum(precio) into toal_precio from product where prod_id = :new.prod_id;
 insert into pedido values(:old.card_id,toal_precio);
 end total_upd;



-- select sum(precio) into toal_precio from product where prod_id = :new.prod_id;
create or replace function total (nro in  product.prod_id%type)
return decimal as resultado pedido.precio%type;
begin
select sum(pr.precio) into resultado from product pr,pedido pe where pe.cli_id in ( select ls.cli_id from list_prodAux ls);
return resultado;
end total;

select total(1) from dual