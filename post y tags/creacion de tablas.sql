create table posts(
id_post int
-- contenido..
);


alter table posts add constraint pk_posts primary key (id_post);

create table post_tag(
id_tag int,
id_post int
);

alter table post_tag add constraint pk_post_tag primary key (id_tag,id_post);

create table tag(
id_tag int,
descrip varchar(10)
);

alter table tag add constraint pk_tag primary key (id_tag);


alter table post_tag add constraint fk_tag foreign key (id_tag) references tag(id_tag);

alter table post_tag add constraint fk_post foreign key (id_post) references posts(id_post);
