drop table cant_solicitudes;

drop table suma_disponible;

drop table cli_banco;

drop table cuenta;

drop table solicitud_aprobada;

drop table solicitud_negada;

drop table formulario_prestamo;

drop sequence sq_solic;

drop trigger upd_inc_solic;

drop trigger descontar_monto;

drop function monto_actual;

drop function meses_ant;

drop function aprobacion;

drop procedure ins_neg;

drop procedure ins_aprov;

drop procedure crear_solic;