-- contar solicitud de pedido..

create table cant_solicitudes(
nro int
);

CREATE SEQUENCE sq_solic
  START WITH 1 
  INCREMENT BY 1 
  MINVALUE 0;


--primera y unica carga incial :

insert into cant_solicitudes values(0);


-- incrementar la cantidad de solic.:
--drop trigger upd_inc_solic

create or replace trigger upd_inc_solic
before insert on solicitud_aprobada
for each row
begin
update cant_solicitudes set nro = sq_solic.nextval;
end upd_inc_solic;


-- testear:

insert into solicitud_prestamo values(1,23,200.89);
insert into solicitud_prestamo values(2,21,300.89);


-- creat un aprobador de solicitudes:

--para eso es necesario hace una funcion que retorne si el cliente solicitate esta en condiciones de pedir un prestamos.
-- para decidir si un cliente eta en condicions de pedir un prestamo se debe saber meses de antiguedad en una empresa.
-- si los meses superan 1 a�o el presstamo se realizara, de lo contrario el prestamo sera negado.
-- con un formulario que debe llenar antes de hacer la solic
-- creat formulario:

create table formulario_prestamo(
cli_id int,
cuenta_dest int,
meses_antiguedad int,
monto decimal(20)
--aprobacion boolean
);

-- crear una fncion que devuelva falso o verdadero si el prestamo se realiza o no.


create or replace function aprobacion(meses in  formulario_prestamo.meses_antiguedad%type)
return char as resultado solicitud_aprobada.solic_completada%type;
begin

if meses >= 12 then resultado:= 'Y';
else resultado:= 'N';
end if;
return resultado;
end aprobacion;

select aprobacion(12) from dual;

-- crear disparador para el formulario que afecte al la tabla de solicitudes, de lo contrario afecta a las solic reachazadar.
drop trigger crear_solic

create or replace procedure crear_solic (nro number) as

meses number;
monto decimal;
cuenta_dest number;

begin
meses:= meses_ant(nro);
select fr.monto into monto from formulario_prestamo fr where fr.cli_id = nro;
select fr.cuenta_dest into cuenta_dest from formulario_prestamo fr where fr.cli_id = nro;
if aprobacion(meses) = 'Y' then ins_aprov(nro,cuenta_dest,monto);--insert into solicitud_aprobada values(:new.cli_id, :new.cuenta_dest,:new.monto,'N');
elsif aprobacion(meses) = 'N' then ins_neg(nro, cuenta_dest,monto);
end if;
end crear_solic;

create or replace procedure ins_aprov(cli_id in solicitud_aprobada.cli_id%type,cuenta_dest in solicitud_aprobada.cuenta_dest%type,
monto in  solicitud_aprobada.monto%type) as
begin
insert into solicitud_aprobada values(cli_id,cuenta_dest,monto,'N');
end ins_aprov;

create or replace procedure ins_neg(cli_id in solicitud_negada.cli_id%type,cuenta_dest in solicitud_negada.cuenta_dest%type,
monto in  solicitud_negada.monto%type) as
begin
insert into solicitud_negada values(cli_id,cuenta_dest,monto);
end ins_neg;


create or replace function meses_ant(nro in formulario_prestamo.cli_id%type)
return int as resultado formulario_prestamo.meses_antiguedad%type;
begin
select meses_antiguedad into resultado from formulario_prestamo fr where fr.cli_id = nro;
return resultado;
end meses_ant;





--testear :

insert into formulario_prestamo values(1,23,12,200);
insert into formulario_prestamo values(2,21,5,200);

call crear_solic(1);
call crear_solic(2);


select * from solicitud_aprobada
select * from solicitud_negada

--- test : OK

-- una vez se registre un solicitud aprobada de prestamo se debe descontar dinero
-- del total disponible para realizar prestamos.
-- para esto se propone que cunado se registre una nueva solicitud ss debera efectuar la operacion previamete
-- mencionada de en la tabla 'suma_disponible'

-- la tabla 'suma_disponible' dispone un campo tipo timestamp que por cada prestamo que se realiza
-- se debe insertar un registro que guarde el monto disponible actual

create or replace trigger descontar_monto
before insert on solicitud_aprobada
for each row
declare
begin
insert into suma_disponible values (sysdate,monto_actual(:new.monto));
end  desocntar_monto;


-- crear funcion que devuelva el monto actualizado:

insert into suma_disponible values(sysdate, 400.76) ;

create or replace function monto_actual(monto in solicitud_aprobada.monto%type)
return decimal as resultado suma_disponible.monto_disponible%type;
begin
select monto_disponible - monto into resultado from suma_disponible where hasta_fecha in (select max(hasta_fecha) from suma_disponible);
return resultado;
end monto_actual;

-- testaer :

-- hay q insertar u reg para este ej donde ya disponemos de monto inicial para hacer prestamos:

insert into suma_disponible values(sysdate, 10000);

-- ahora podemos saber de cuanto dinero disponemos a medida que hacemos prestamos:

select * from suma_disponible

-- hasta_fecha es un campo que nos va a decir la fecha hasta el ultimo prestamo


