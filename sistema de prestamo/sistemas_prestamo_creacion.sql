-- modelar DER para sistema de prestamos de un banco..

create table cli_banco(
cli_id int,
cuenta int
);

alter table cli_banco add constraint pk_cli_banco primary key (cli_id);


create table cuenta(
cuenta int,
monto decimal(10)
);

alter table cuenta add constraint pk_cuenta primary key (cuenta);


create table solicitud_aprobada(
cli_id int,
cuenta_dest int,
monto decimal(10),
solic_completada char(1)
);

alter table solicitud_aprobada add constraint pk_cuenta primary key (cli_id,cuenta_dest);


create table solicitud_negada(
cli_id int,
cuenta_dest int,
monto decimal(10)
);

alter table solicitud_negada add constraint pk_cuenta primary key (cli_id,cuenta_dest);


create table suma_disponible(
hasta_fecha timestamp,
monto_disponible decimal(20)
);


/*
create table (
cli_id int,
monto int
);
*/